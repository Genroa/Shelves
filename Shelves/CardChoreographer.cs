using SecretHistories.UI;
using UnityEngine;

namespace Shelf
{
    public static class ShelfChoreographer
    {
        public static void RepositionToken(Token token, Vector2 position)
        {
            // In the future, we can add the option to use Itinerary to animate cards traveling.
            // ShelfAreas will probably need to be aware of per-card slots by that point, so we do not
            // end up sending several cards to the same location
            if (token.Sphere != GameAPI.GetTabletopSphere())
            {
                NoonUtility.LogWarning($"Refusing to move token {token.PayloadId} that is not in the tabletop sphere");
                return;
            }

            token.gameObject.transform.localPosition = position;
        }
    }
}