﻿using Roost.Twins.Entities;
using Roost.World;
using SecretHistories.Entities;
using SecretHistories.Fucine;
using SecretHistories.Fucine.DataImport;
using SecretHistories.Services;
using SecretHistories.UI;
using Shelf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shelf.Entities
{
    // AreaStyle values: stretch, aligncenter, repeat
    public class ShelfArea : AbstractEntity<ShelfArea>
    {
        [FucineValue(DefaultValue = 1)] public int X { get; set; }
        [FucineValue(DefaultValue = 1)] public int Y { get; set; }
        [FucineValue(DefaultValue = 1)] public int Rows { get; set; }
        [FucineValue(DefaultValue = 1)] public int Columns { get; set; }
        [FucineValue(DefaultValue = "")] public string Background { get; set; }
        [FucineEverValue(DefaultValue = "aligncenter")] public string Style { get; set; }
        [FucineEverValue(DefaultValue = FucineExp<bool>.UNDEFINED)] public FucineExp<bool> Expression { get; set; }

        public ShelfArea(EntityData importDataForEntity, ContentImportLog log) : base(importDataForEntity, log)
        {
        }

        public bool CanAcceptToken(Token token)
        {
            // Note: Not sure of the implications of doing this per token vs doing it bulk on all tokens, but being able to query a single token in an area gives us
            // some benefits elsewhere.
            return FucineCache.FilterTokens(new List<Token>(new[] { token }), Expression).Any();
        }

        protected override void OnPostImportForSpecificEntity(ContentImportLog log, Compendium populatedCompendium) { }
    }
}
