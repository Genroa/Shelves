using NCalc;
using Roost.Twins.Entities;
using SecretHistories.Fucine;
using SecretHistories.Fucine.DataImport;
using SecretHistories.UI;
using Shelf;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shelf.Entities
{
    /*
     * A Shelf is an area:
     * - defined by N rows and M columns, 1, 1 by default
     * - an optional parent offset, 0 0 by default
     * - a filter property, to define the elements moved to here
     * - children areas
     * - an outline colour
     */
    [FucineImportable("shelves")]
    public class ShelfEntity : AbstractEntity<ShelfEntity>
    {
        [FucineValue(DefaultValue = "")]
        public string Category { get; set; }

        [FucineValue(DefaultValue = "", Localise = true)]
        public string Name { get; set; }

        [FucineList]
        public List<string> Legacies { get; set; }

        [FucineValue(DefaultValue = "", Localise = true)]
        public string Icon { get; set; }

        [FucineValue(DefaultValue = 1)]
        public int Rows { get; set; }

        [FucineValue(DefaultValue = 1)]
        public int Columns { get; set; }

        [FucineValue(DefaultValue = "")]
        public string Background { get; set; }

        [FucineEverValue(DefaultValue = "aligncenter", Localise = true)]
        public string Style { get; set; }

        [FucineEverValue(DefaultValue = FucineExp<bool>.UNDEFINED)]
        public FucineExp<bool> Expression { get; set; }

        [FucineList]
        public List<ShelfArea> Areas { get; set; }

        [FucineList(DefaultValue = null)]
        public List<Dictionary<string, FucineExp<int>>> RootCache { get; set; }

        [FucineValue(DefaultValue = 2)]
        public int Priority { get; set; }

        [FucineValue(DefaultValue = false)]
        public bool NoOutline { get; set; }

        public ShelfEntity(EntityData importDataForEntity, ContentImportLog log) : base(importDataForEntity, log) { }

        public int GetClampedPriority()
        {
            return Math.Clamp(Priority, 0, 4);
        }

        public Sprite GetIconSprite()
        {
            if (!string.IsNullOrEmpty(Icon))
            {
                return ResourcesManager.GetSpriteForUI(Icon);
            }

            return null;

        }

        public IEnumerable<Token> FilterTokens(IEnumerable<Token> tokens)
        {
            try
            {
                return FucineCache.FilterTokens(tokens, Expression);
            }
            catch (EvaluationException e)
            {
                throw new ShelfExpressionException(Id, Expression.formula, e);
            }
        }

        protected override void OnPostImportForSpecificEntity(ContentImportLog log, Compendium populatedCompendium)
        {
        }
    }
}
