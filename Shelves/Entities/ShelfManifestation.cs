﻿using SecretHistories;
using SecretHistories.Abstract;
using SecretHistories.Enums;
using SecretHistories.Ghosts;
using SecretHistories.Manifestations;
using SecretHistories.Spheres;
using SecretHistories.UI;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using SecretHistories.Services;
using SecretHistories.Spheres.Choreographers;
using NCalc;
using Roost.Piebald;
using Shelf;
using Roost;

namespace Shelf.Entities
{
    [RequireComponent(typeof(RectTransform))]
    class ShelfManifestation : BasicManifestation, IManifestation, IComparable<ShelfManifestation>
    {
        public static float CellWidth = 75;
        public static float CellHeight = 115;

        LineRenderer lr = null;
        public ShelfEntity Entity = null;
        public Token Token;

        Sphere tabletop;

        public bool RequestingNoDrag => AreShelvesLockedInPlace();
        public bool RequestingNoSplit => true;

        public bool NoPush => true;

        private SizedItemWidget editingLayout;
        private SizedItemWidget shelfArtLayout;

        private float ShelfWidth => Entity.Columns * CellWidth;
        private float ShelfHeight => Entity.Rows * CellHeight;

        public void Initialise(IManifestable manifestable)
        {
            tabletop = GameAPI.GetTabletopSphere();
            Token = manifestable.GetToken();

            // Here we build the actual visual representation of the zone, based on the ZoneData we receive in arg
            var sp = manifestable as ShelfPayload;
            gameObject.name = $"shelf_{sp.EntityId}_manifestation";

            Entity = Watchman.Get<Compendium>().GetEntityById<ShelfEntity>(sp.EntityId);

            RectTransform.anchoredPosition = Vector2.zero;
            RectTransform.sizeDelta = new Vector2(ShelfWidth, ShelfHeight);

            WidgetMountPoint.On(RectTransform, mountPoint =>
            {
                shelfArtLayout = mountPoint.AddSizedItem("ShelfArt");
                BuildMainShelf(shelfArtLayout);
                for (int i = 0; i < Entity.Areas.Count; i++)
                {
                    ShelfArea area = Entity.Areas[i];
                    BuildArea(area, i, shelfArtLayout);
                }

                editingLayout = mountPoint.AddSizedItem("EditingUI")
                    .SetActive(ShelfUI.IsOpen)
                    .AddContent(mountPoint =>
                    {
                        mountPoint.AddImage("EditingBackdrop")
                            .SetColor(new Color(1, 1, 1, 0.5f))
                            .SetSprite("shelf_editing_backdrop");

                        var deleteButtonSize = 32;
                        mountPoint.AddIconButton("CloseButton")
                            .SetLeft(1, -13 - deleteButtonSize)
                            .SetRight(1, -13)
                            .SetTop(1, -13)
                            .SetBottom(1, -13 - deleteButtonSize)
                            .SetSprite("shelf_delete_icon")
                            .CenterImage()
                            .SetClickSound("UIButtonClose")
                            .OnClick(() => Shelves.DisableShelf(Entity.Id));
                    });


            });


            ShelfUI.UIToggled += OnUIToggled;
            OnUIToggled(null, EventArgs.Empty);

            Shelves.RegisteredShelfManifestations.Add(this);
        }

        public IGhost CreateGhost()
        {
            return NullGhost.Create(this);
        }

        public OccupiesSpaceAs OccupiesSpaceAs() => SecretHistories.Enums.OccupiesSpaceAs.Meta;

        public void Emphasise() { }
        public void Understate() { }

        public void Highlight(HighlightType highlightType, IManifestable manifestable) { }
        public void Unhighlight(HighlightType highlightType, IManifestable manifestable) { }

        public void Shroud(bool instant) { }
        public void Unshroud(bool instant) { }

        public void UpdateVisuals(IManifestable manifestable, Sphere sphere)
        {
            if (ShelfUI.IsOpen)
            {
                return;
            }

            // Important, to never be on top of a card after being dropped, just like dropzones.
            gameObject.transform.parent.SetAsFirstSibling();
        }

        public override void UpdateStateAimedAtSphere(IManifestable manifestable, Sphere sphere)
        {
            if (ShelfUI.IsOpen)
            {
                return;
            }

            gameObject.transform.parent.SetAsFirstSibling();
        }

        public override bool HandlePointerClick(PointerEventData eventData, Token token)
        {
            return false;
        }

        public int CompareTo(ShelfManifestation other)
        {
            var result = Entity.GetClampedPriority().CompareTo(other.Entity.GetClampedPriority());
            if (result == 0)
            {
                // Fall back on IDs for determinism.
                result = Entity.Id.CompareTo(other.Entity.Id);
            }

            return result;
        }

        public override void Retire(RetirementVFX vfx, Action callbackOnRetired)
        {
            Destroy(gameObject);
            callbackOnRetired();
        }

        private void OnDestroy()
        {
            ShelfUI.UIToggled -= OnUIToggled;
            Shelves.RegisteredShelfManifestations.Remove(this);
        }

        private void Update()
        {
            SetOutlinePositions();
        }

        private void OnUIToggled(object sender, EventArgs e)
        {
            editingLayout.SetActive(ShelfUI.IsOpen);

            // Drop us on top of the cards, so our UI is visible.
            if (ShelfUI.IsOpen)
            {
                gameObject.transform.parent.SetAsLastSibling();
                OnEnterEditorMode();
            }
            else
            {
                gameObject.transform.parent.SetAsFirstSibling();
                OnLeaveEditorMode();
            }
        }

        private void OnEnterEditorMode()
        {
            object obj = Watchman.Get<Config>().GetConfigValue("hideshelves");
            bool hideShelvesBehaviour = obj != null && obj.ToString().Equals("1");

            if(hideShelvesBehaviour)
            {
                MakeVisible();
            }
            else
            {
                foreach (var image in shelfArtLayout.GameObject.GetComponentsInChildren<Image>())
                {
                    image.color = new Color32(255, 255, 255, 255);
                }
            }
        }

        private void OnLeaveEditorMode()
        {
            object obj = Watchman.Get<Config>().GetConfigValue("hideshelves");
            bool hideShelvesBehaviour = obj != null && obj.ToString().Equals("1");

            if (hideShelvesBehaviour)
            {
                MakeInvisible();
            }
            else
            {
                foreach (var image in shelfArtLayout.GameObject.GetComponentsInChildren<Image>())
                {
                    image.color = new Color32(255, 255, 255, 50);
                }
            }
        }

        public void MakeVisible()
        {
            if(lr != null) lr.enabled = true;
            Token.MakeVisible();
        }

        public void MakeInvisible()
        {
            if (lr != null) lr.enabled = false;
            Token.MakeInvisible();
        }

        private void BuildMainShelf(WidgetMountPoint mountPoint)
        {
            if (!Entity.NoOutline)
            {
                BuildOutline();
            }

            AddBackgroundImageComponent(
                mountPoint,
                Entity.Background != "" ? Entity.Background : "empty_bg",
                Entity.Style,
                new Color32(255, 255, 255, 50)
            );
        }

        void BuildArea(ShelfArea area, int index, WidgetMountPoint mountPoint)
        {
            // areaRect is in relative-to-shelf coords, so we can use it directly.
            var areaRect = GetAreaRect(area);

            var shelfArea = mountPoint.AddSizedItem($"shelfarea_{index}")
                .SetBottom(.5f, areaRect.yMin)
                .SetTop(.5f, areaRect.yMax)
                .SetLeft(.5f, areaRect.xMin)
                .SetRight(.5f, areaRect.xMax);

            if (area.Background != "")
            {
                AddBackgroundImageComponent(
                    shelfArea,
                    area.Background,
                    area.Style,
                    new Color32(255, 255, 255, 50)
                );
            }
        }

        /// <summary>
        /// Gets a rect of the total shelf area in tabletop coordinates
        /// </summary>
        private Rect GetShelfRect()
        {
            // Sometimes, we can be in the dragging sphere.  Make sure its relative to tabletop.
            return Token.GetRectInOtherSphere(tabletop);
        }

        /// <summary>
        /// Gets a rect of the area in shelf coordinates
        /// </summary>
        private Rect GetAreaRect(ShelfArea area)
        {
            var shelfRect = GetShelfRect();
            // Our origin is in the center of the shelf, so take that into account
            // when determining the shelf coordinate of the area.
            var rect = new Rect(
                (area.X - 1) * CellWidth - shelfRect.width / 2,
                (Entity.Rows - area.Y) * CellHeight - shelfRect.height / 2,
                area.Columns * CellWidth,
                area.Rows * CellHeight
            );
            return rect;
        }

        /// <summary>
        /// Gets a rect of the cell in shelf coordinates.
        /// </summary>
        private Rect GetCellRect(ShelfArea area, int posX, int posY)
        {
            var areaRect = GetAreaRect(area);

            var firstCellBottomLeft = new Vector2(areaRect.xMin, areaRect.yMin);
            var cellBottomLeft = firstCellBottomLeft + new Vector2(posX * CellWidth, posY * CellHeight);

            return new Rect(cellBottomLeft.x, cellBottomLeft.y, CellWidth, CellHeight);
        }

        private Rect ToTableRect(Rect shelfRelativeRect)
        {
            var shelfPos = Token.GetRectInCurrentSphere().center;
            var shelfRectPos = new Vector2(shelfRelativeRect.x, shelfRelativeRect.y);
            var tableRectPos = shelfPos + shelfRectPos;
            return new Rect(tableRectPos.x, tableRectPos.y, shelfRelativeRect.width, shelfRelativeRect.height);
        }

        void BuildOutline()
        {
            gameObject.AddComponent<LineRenderer>();
            lr = gameObject.GetComponent<LineRenderer>();

            lr.positionCount = 5;
            SetOutlinePositions();

            lr.material = new Material(Shader.Find("UI/Default"));
            lr.material.color = new Color32(255, 255, 255, 100);
            lr.startWidth = 1.5f;
            lr.sortingOrder = 1;
            lr.alignment = LineAlignment.TransformZ;
            lr.loop = true;
        }

        void SetOutlinePositions()
        {
            // This builds lines in world coordinates, so we need the rect according to the world
            var rect = Token.GetRectFromPosition(Token.transform.position);

            var h = Token.transform.position.z;

            lr.SetPosition(0, new Vector3(rect.xMin, rect.yMin, h));
            lr.SetPosition(1, new Vector3(rect.xMax, rect.yMin, h));
            lr.SetPosition(2, new Vector3(rect.xMax, rect.yMax, h));
            lr.SetPosition(3, new Vector3(rect.xMin, rect.yMax, h));
            lr.SetPosition(4, new Vector3(rect.xMin, rect.yMin, h));
        }

        void AddBackgroundImageComponent(WidgetMountPoint mountPoint, string bg, string style, Color32 color)
        {
            Sprite sprite = ResourcesManager.GetSpriteForUI(bg);
            if (sprite == null)
            {
                NoonUtility.LogWarning($"UI Sprite {bg} not found.  Falling back to empty_bg");
                sprite = ResourcesManager.GetSpriteForUI("empty_bg");
            }

            var image = mountPoint.AddImage("background")
                .SetSprite(sprite)
                .SetColor(color)
                .SetPivot(0.5f, 0.5f);

            if (style == "repeat")
            {
                image.TileImage();
                image.SetPixelsPerUnit(2.5f);
            }
            else if (style == "aligncenter")
            {
                image.CenterImage();
            }
            else
            {
                image.StretchImage();
            }
        }

        public override Vector2 GetGridOffset(Vector2 gridCellSize, Sphere forSphere)
        {
            // Logic loosely cribbed from DropZone when configured for Element targets.
            // This is resposible for aligning the snapping of our shelves into the same grid that Element tokens snap to.
            return new Vector2(RectTransform.sizeDelta.x / 2.0f - gridCellSize.x / 2.0f, RectTransform.sizeDelta.y / 2.0f - gridCellSize.y / 2.0f);
        }

        public ShelfSortResult ArrangeToken(Token token)
        {
            var tokenResult = ShelfSortResult.UnknownToken;

            foreach (var area in Entity.Areas)
            {
                var result = TryPlaceToken(area, token);
                if (result > tokenResult)
                {
                    tokenResult = result;
                }

                // Note: We used to remove the area if it was blocked, but that will result in us not telling other tokens that
                // they too were  blocked, and thus let them think they are fine where they are / do not want to sort.

                if (result.Sorted)
                {
                    // We found a place for this token.
                    // Break the loop and tell upstream we sorted it.
                    return tokenResult;
                }
            }

            return tokenResult;
        }

        public LegalPositionCheckResult IsLegalPlacement(Rect candidateRect, Token placingToken)
        {
            //Is the candidaterect inside the larger tabletop rect? if not, throw it out now.
            if (!tabletop.GetRect().Overlaps(candidateRect))
            {
                return LegalPositionCheckResult.Illegal();
            }

            Rect otherTokenOverlapRect;

            foreach (var otherToken in tabletop.Tokens.Where(t => t != placingToken && t.OccupiesSameSpaceAs(placingToken) && !CanTokenBeIgnored(t)))
            {
                otherTokenOverlapRect = otherToken.GetRectInCurrentSphere();
                if (otherTokenOverlapRect.Overlaps(candidateRect))
                    return LegalPositionCheckResult.Blocked(otherToken, otherTokenOverlapRect);
            }
            return LegalPositionCheckResult.Legal();
        }
        public virtual bool CanTokenBeIgnored(Token token)
        {
            return token.Defunct || token.NoPush;
        }

        public bool IsPositionAvailable(Token token, Vector2 position)
        {
            var tokenRect = token.GetRectFromPosition(position);
            var legalPositionCheckResult = IsLegalPlacement(tokenRect, token);
            return legalPositionCheckResult.IsLegal;
        }

        private Vector2 NextAvailablePosition(ShelfArea area, Token token)
        {
            for (var y = 0; y < area.Rows; y++)
            {
                for (var x = 0; x < area.Columns; x++)
                {
                    // Get the tabletop position of the cell.
                    var origin = ToTableRect(GetCellRect(area, x, y)).center;

                    // Is legal? If so, return. If not, continue
                    if (IsPositionAvailable(token, origin))
                    {
                        return origin;
                    }
                }
            }

            return Vector2.negativeInfinity;
        }

        private ShelfSortResult TryPlaceToken(ShelfArea area, Token token)
        {

            // Note: Fucine expressions really want to bulk run on tokens, but inverting the SortTokens loop
            // causes all sorts of nastiness to determine what the last result was for the token.
            // Trying it this way for now.  If there is good reason to bulk sort tokens, we can
            // invert this loop by using a dictionary to track what the best result was per token.
            // TODO: This is expensive.  If we could get away with not having to tell the difference between UnknownToken and TokenBlocked,
            // we could check for no NextAvailablePosition first and return TokenBlocked immediately.
            try
            {
                if (!area.CanAcceptToken(token))
                {
                    // This area can't accept this token.
                    return ShelfSortResult.UnknownToken;
                }
            }
            catch (EvaluationException e)
            {
                throw new ShelfExpressionException(Entity.Id, area.Expression.formula, e);
            }

            if (Token.CurrentState.InPlayerDrivenMotion() || Token.CurrentState.InSystemDrivenMotion() || Token.Sphere != tabletop)
            {
                // We are being dragged around.  We can accept this token, but can't place it at the moment.
                // Report it as blocked so it knows to try again later.
                return ShelfSortResult.TokenBlocked;
            }

            // Get the table rect of our area and compare it to where the token currently is.
            var areaRect = ToTableRect(GetAreaRect(area));
            var tokenRect = token.GetRectInCurrentSphere();

            if (areaRect.Overlaps(tokenRect))
            {
                // Already where it should be.
                return ShelfSortResult.TokenSorted;
            }

            // Should we try to stack tokens that are already in the area?

            var newPos = NextAvailablePosition(area, token);
            if (newPos.Equals(Vector2.negativeInfinity))
            {
                return ShelfSortResult.TokenBlocked;
            }

            ShelfChoreographer.RepositionToken(token, newPos);
            return ShelfSortResult.TokenSorted;
        }

        private static bool AreShelvesLockedInPlace()
        {
            if (ShelfUI.IsOpen)
            {
                return false;
            }

            object obj = Watchman.Get<Config>().GetConfigValue("lockshelves");
            return obj != null && obj.ToString().Equals("1");
        }
    }
}
