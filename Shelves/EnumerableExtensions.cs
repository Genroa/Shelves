using System;
using System.Collections.Generic;

namespace Shelf
{
    public static class EnumerableExensions
    {
        public static int FindIndex<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            if (enumerable is IReadOnlyList<T> list)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (predicate(list[i]))
                    {
                        return i;
                    }
                }

                return -1;
            }

            int index = 0;
            foreach (var item in enumerable)
            {
                if (predicate(item))
                {
                    return index;
                }
                index++;
            }
            return -1;
        }

        public static IEnumerable<T> DistinctBy<T, TValue>(this IEnumerable<T> enumerable, Func<T, TValue> selector)
        {
            var set = new HashSet<TValue>();
            foreach (var item in enumerable)
            {
                var value = selector(item);
                if (set.Add(value))
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> enumerable, int count)
        {
            var window = new List<T>(count);
            foreach (var item in enumerable)
            {
                window.Add(item);
                if (window.Count == count)
                {
                    yield return window;
                    window.Clear();
                }
            }

            if (window.Count > 0)
            {
                yield return window;
            }
        }
    }
}
