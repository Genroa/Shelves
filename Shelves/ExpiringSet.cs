using System;
using System.Collections;
using System.Collections.Generic;

namespace Shelf
{
    public class ExpiringSet<T> : ICollection<T>
    {
        private readonly Dictionary<T, DateTime> items = new Dictionary<T, DateTime>();

        public ExpiringSet(TimeSpan expiration, TimeSpan fudgeFactor)
        {
            DefaultExpiration = expiration;
            DefaultFudgeFactor = fudgeFactor;
        }

        public TimeSpan DefaultExpiration { get; set; }

        public TimeSpan DefaultFudgeFactor { get; set; }


        public int Count => items.Count;

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            items[item] = DateTime.Now + DefaultExpiration + DefaultFudgeFactor * UnityEngine.Random.Range(0, 1f);
        }

        public void Add(T item, TimeSpan expiration, TimeSpan? fudgeFactor = null)
        {
            items[item] = DateTime.Now + expiration + (fudgeFactor ?? DefaultFudgeFactor) * UnityEngine.Random.Range(0, 1f);
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(T item)
        {
            Prune();
            return items.ContainsKey(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Prune();
            items.Keys.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            Prune();
            return items.Keys.GetEnumerator();
        }

        public bool Remove(T item)
        {
            return items.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void Prune()
        {
            var now = DateTime.Now;
            var toRemove = new List<T>();

            foreach (var item in items)
            {
                if (item.Value < now)
                {
                    toRemove.Add(item.Key);
                }
            }

            foreach (var item in toRemove)
            {
                items.Remove(item);
            }
        }
    }
}
