using System;
using System.Collections.Generic;
using System.Linq;
using Roost.Twins.Entities;
using Roost.World;
using SecretHistories.UI;

namespace Shelf
{
    public static class FucineCache
    {
        private static readonly TimeSpan CacheLifetime = TimeSpan.FromMinutes(5);
        private static readonly TimeSpan CleanInterval = TimeSpan.FromMinutes(1);

        private static Dictionary<string, ExpressionCacheItem> ExpressionCache = new();

        private static DateTime lastCleanTime = DateTime.Now;

        public static void Clear()
        {
            lastCleanTime = DateTime.Now;
            ExpressionCache.Clear();
        }

        public static IEnumerable<Token> FilterTokens(IEnumerable<Token> tokens, FucineExp<bool> expression)
        {
            if (DateTime.Now - lastCleanTime > CleanInterval)
            {
                lastCleanTime = DateTime.Now;
                Clean();
            }

            var expireTime = DateTime.Now + CacheLifetime;

            // We encounter these a lot, so let's just short circuit them.
            if (string.IsNullOrEmpty(expression.formula))
            {
                foreach (var token in tokens)
                {
                    yield return token;
                }

                yield break;
            }

            var cache = GetExpressionCache(expression);
            var misses = new List<Token>();
            foreach (var token in tokens)
            {
                var sig = token.Payload.GetSignature();
                if (!cache.TokenResultsBySig.TryGetValue(sig, out var result))
                {
                    misses.Add(token);
                    continue;
                }

                // Token was used, update the expire time.
                result.ExpirationTime = expireTime;

                if ((bool)result.Result)
                {
                    yield return token;
                }
            }

            if (misses.Count == 0)
            {
                // FilterTokens is heavy even with zero items.
                yield break;
            }


            // Note: This grabs the signatures again for all missed tokens... Seems to be decently fast though.
            var unfiltered = new HashSet<Token>(misses);
            foreach (var token in misses.FilterTokens(expression))
            {
                unfiltered.Remove(token);

                cache.TokenResultsBySig[token.Payload.GetSignature()] = new ExpressionTokenCacheItem
                {
                    Result = true,
                    ExpirationTime = expireTime
                };

                yield return token;
            }

            foreach (var token in unfiltered)
            {
                cache.TokenResultsBySig[token.Payload.GetSignature()] = new ExpressionTokenCacheItem
                {
                    Result = false,
                    ExpirationTime = expireTime
                };
            }
        }

        private static ExpressionCacheItem GetExpressionCache<T>(FucineExp<T> expression)
            where T : IConvertible
        {
            if (!ExpressionCache.TryGetValue(expression.formula, out var cache))
            {
                cache = new ExpressionCacheItem();
                ExpressionCache.Add(expression.formula, cache);
            }

            return cache;
        }

        private static void Clean()
        {
            var now = DateTime.Now;
            foreach (var key in ExpressionCache.Keys.ToArray())
            {
                var item = ExpressionCache[key];
                CleanItem(item);
                if (item.TokenResultsBySig.Count == 0)
                {
                    ExpressionCache.Remove(key);
                }
            }
        }

        private static void CleanItem(ExpressionCacheItem item)
        {
            foreach (var key in item.TokenResultsBySig.Keys.ToArray())
            {
                var tokenResult = item.TokenResultsBySig[key];
                if (tokenResult.ExpirationTime < DateTime.Now)
                {
                    item.TokenResultsBySig.Remove(key);
                }
            }
        }

        private class ExpressionCacheItem
        {
            public Dictionary<string, ExpressionTokenCacheItem> TokenResultsBySig { get; } = new();
        }

        private struct ExpressionTokenCacheItem
        {
            public object Result { get; set; }
            public DateTime ExpirationTime { get; set; }
        }
    }
}