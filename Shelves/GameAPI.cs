using System;
using System.Linq;
using Roost;
using SecretHistories.Entities;
using SecretHistories.Entities.NullEntities;
using SecretHistories.Infrastructure;
using SecretHistories.Infrastructure.Persistence;
using SecretHistories.Spheres;
using SecretHistories.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Shelf
{
    public static class GameAPI
    {
        private static Sphere tabletopSpereCache = null;

        public static event EventHandler GameStarted;
        public static event EventHandler GameEnded;

        public static event EventHandler<GameLoadRequestedEventArgs> GameLoadRequested;

        public static void Initialise()
        {
            Machine.Patch(
                original: typeof(GameGateway).GetMethodInvariant(nameof(GameGateway.LoadGame)),
                postfix: typeof(GameAPI).GetMethodInvariant(nameof(OnGameLoadRequested))
            );

            SceneManager.sceneLoaded += new UnityAction<Scene, LoadSceneMode>(OnSceneLoaded);
            SceneManager.sceneUnloaded += new UnityAction<Scene>(OnSceneUnloaded);
        }

        public static Sphere GetTabletopSphere()
        {
            if (tabletopSpereCache == null)
            {
                tabletopSpereCache = (Sphere)Watchman.Get<HornedAxe>().GetSpheres().OfType<TabletopSphere>().FirstOrDefault() ?? new NullSphere();
            }

            return tabletopSpereCache;
        }

        private static void OnGameLoadRequested(GamePersistenceProvider gamePersistenceProvider)
        {
            GameLoadRequested?.Invoke(null, new GameLoadRequestedEventArgs(gamePersistenceProvider));
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (scene.name == "S4Tabletop")
            {
                tabletopSpereCache = null;
                GameStarted?.Invoke(null, EventArgs.Empty);
            }
        }

        private static void OnSceneUnloaded(Scene scene)
        {
            if (scene.name == "S4Tabletop")
            {
                tabletopSpereCache = null;
                GameEnded?.Invoke(null, EventArgs.Empty);
            }
        }

        public class GameLoadRequestedEventArgs : EventArgs
        {
            public GameLoadRequestedEventArgs(GamePersistenceProvider provider)
            {
                this.Provider = provider;
            }

            public GamePersistenceProvider Provider { get; }
        }
    }
}