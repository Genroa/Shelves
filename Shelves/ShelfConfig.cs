using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SecretHistories.Entities;
using SecretHistories.UI;
using UnityEngine;

namespace Shelf
{
    public static class ShelfConfig
    {
        private static ShelfConfigRoot ConfigRoot;

        private static string ConfigPath
        {
            get
            {
                return Path.Combine(Watchman.Get<MetaInfo>().PersistentDataPath, "./shelves.json");
            }
        }

        public static string ActiveLayout
        {
            get
            {
                return $"legacy:{Watchman.Get<Stable>().Protag().ActiveLegacyId}";
            }
        }

        public static event EventHandler<ShelfToggledEventArgs> ShelfToggled;

        public static bool IsShelfEnabled(string shelfId, string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            return layoutItem.shelves.ContainsKey(shelfId);
        }

        public static IReadOnlyCollection<string> GetEnabledShelves(string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            return layoutItem.shelves.Keys.ToList();
        }

        public static Vector2 GetShelfPosition(string shelfId, string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            if (!layoutItem.shelves.TryGetValue(shelfId, out var shelfConfigItem))
            {
                return Vector2.zero;
            }

            return new Vector2(shelfConfigItem.x, shelfConfigItem.y);
        }

        public static Vector2 GetDropzonePosition(string dropzoneId, string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            if (!layoutItem.dropzones.TryGetValue(dropzoneId, out var dropzoneConfigItem))
            {
                return Vector2.zero;
            }

            return new Vector2(dropzoneConfigItem.x, dropzoneConfigItem.y);
        }

        public static void SetDropzonePosition(string dropzoneId, Vector2 position, string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            if (!layoutItem.dropzones.TryGetValue(dropzoneId, out var dropzoneConfigItem))
            {
                dropzoneConfigItem = new PositionConfigItem();
                layoutItem.dropzones.Add(dropzoneId, dropzoneConfigItem);
            }

            dropzoneConfigItem.x = position.x;
            dropzoneConfigItem.y = position.y;

            StoreConfig();
        }


        public static void DeleteShelf(string shelfId, string layout = null)
        {
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            layoutItem.shelves.Remove(shelfId);
            ShelfToggled?.Invoke(null, new ShelfToggledEventArgs(shelfId, false));

            StoreConfig();
        }

        public static void SetShelfPosition(string shelfId, Vector2 position, string layout = null)
        {
            var wasEnabled = false;
            var layoutItem = GetLayout(layout ?? ActiveLayout);

            if (!layoutItem.shelves.TryGetValue(shelfId, out var shelfConfigItem))
            {
                shelfConfigItem = new PositionConfigItem();
                layoutItem.shelves.Add(shelfId, shelfConfigItem);
                wasEnabled = true;
            }

            shelfConfigItem.x = position.x;
            shelfConfigItem.y = position.y;

            if (wasEnabled)
            {
                ShelfToggled?.Invoke(null, new ShelfToggledEventArgs(shelfId, true));
            }

            StoreConfig();
        }

        private static ShelfLayoutItem GetLayout(string layoutId)
        {
            if (layoutId == "default")
            {
                NoonUtility.LogWarning("Using default layout, this is probably not what you want.  From: " + new StackTrace().ToString());
            }

            EnsureConfigLoaded();

            if (!ConfigRoot.layouts.TryGetValue(layoutId, out var layout))
            {
                layout = new ShelfLayoutItem();
                ConfigRoot.layouts.Add(layoutId, layout);
            }

            return layout;
        }

        private static void EnsureConfigLoaded()
        {
            if (ConfigRoot != null)
            {
                return;
            }

            if (!File.Exists(ConfigPath))
            {
                ConfigRoot = new ShelfConfigRoot();
                return;
            }

            try
            {
                var serializer = new JsonSerializer();
                using (var streamReader = new StreamReader(ConfigPath))
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    ConfigRoot = serializer.Deserialize<ShelfConfigRoot>(jsonTextReader);
                }
            }
            catch (Exception e)
            {
                NoonUtility.LogWarning($"Failed to load shelf config: {e}");
                ConfigRoot = new ShelfConfigRoot();
            }
        }

        private static void StoreConfig()
        {
            try
            {
                var serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                using (var streamWriter = new StreamWriter(ConfigPath))
                using (var jsonTextWriter = new JsonTextWriter(streamWriter))
                {
                    serializer.Serialize(jsonTextWriter, ConfigRoot);
                }
            }
            catch (Exception e)
            {
                NoonUtility.LogWarning($"Failed to store shelf config: {e}");
            }
        }

        public class ShelfToggledEventArgs : EventArgs
        {
            public ShelfToggledEventArgs(string shelfId, bool isEnabled)
            {
                ShelfId = shelfId;
                IsEnabled = isEnabled;
            }

            public string ShelfId { get; }
            public bool IsEnabled { get; }
        }

        private class ShelfConfigRoot
        {
            public Dictionary<string, ShelfLayoutItem> layouts { get; set; } = new();
        }

        private class ShelfLayoutItem
        {
            public Dictionary<string, PositionConfigItem> dropzones { get; set; } = new();
            public Dictionary<string, PositionConfigItem> shelves { get; set; } = new();
        }

        private class PositionConfigItem
        {
            public float x { get; set; }
            public float y { get; set; }
        }
    }
}
