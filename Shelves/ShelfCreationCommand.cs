﻿using SecretHistories.Abstract;
using SecretHistories.Commands.SituationCommands;
using SecretHistories.Entities.NullEntities;
using SecretHistories.UI;
using System.Collections.Generic;
using Shelf.Entities;

namespace Shelf
{
    class ShelfCreationCommand : ITokenPayloadCreationCommand, IEncaustment
    {
        public string Id { get; set; }

        public string EntityId { get; set; }
        public int Quantity { get; set; }
        public List<PopulateDominionCommand> Dominions { get; set; }

        public ShelfCreationCommand() { }
        public ShelfCreationCommand(string entityId)
        {
            Dominions = new List<PopulateDominionCommand>();
            EntityId = entityId;
            Id = $"shelf_{entityId}";
        }

        public ITokenPayload Execute(Context context)
        {
            if (Watchman.Get<Compendium>().GetEntityById<ShelfEntity>(EntityId) == null) return new NullPayload();
            var z = new ShelfPayload(Id, EntityId);
            return z;
        }
    }
}
