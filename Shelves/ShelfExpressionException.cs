using System;
using NCalc;

namespace Shelf
{
    public class ShelfExpressionException : Exception
    {
        public ShelfExpressionException(string shelfId, string expression, EvaluationException innerException)
            : base($"Error in expression {expression} for shelf {shelfId}: {innerException.Message}", innerException)
        {
            this.ShelfId = shelfId;
            this.Expression = expression;
        }

        public string ShelfId { get; }
        public string Expression { get; }
    }
}