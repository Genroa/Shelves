using System;
using UnityEngine;

public class ShelfHeart : MonoBehaviour
{
    public static event EventHandler OnBeat;

    static ShelfHeart()
    {
        var go = new GameObject();
        go.AddComponent<ShelfHeart>();
        GameObject.DontDestroyOnLoad(go);
    }

    public void Update()
    {
        OnBeat?.Invoke(this, EventArgs.Empty);
    }
}