using System;

namespace Shelf
{
    public class ShelfSortResult : IComparable<ShelfSortResult>
    {
        public enum ShelfSortResultReason
        {
            Unknown,

            /// <summary>
            /// The token was sorted into the shelf area.
            /// </summary>
            Sorted,

            /// <summary>
            /// The token was blocked by another token.
            /// </summary>
            TokenBlocked,
        }

        /// <summary>
        /// Gets a value indicating whether the element is valid for this shelf area.
        /// </summary>
        public bool ValidForShelf { get; private set; } = false;

        /// <summary>
        /// Gets a value indicating whether the element was sorted into this shelf area.
        /// </summary>
        public bool Sorted => Reason == ShelfSortResultReason.Sorted;

        /// <summary>
        /// Gets a value indicating whether the token was blocked from being sorted into this shelf area.
        /// </summary>
        public bool Blocked => Reason == ShelfSortResultReason.TokenBlocked;

        public ShelfSortResultReason Reason { get; private set; } = ShelfSortResultReason.Unknown;

        public static ShelfSortResult UnknownToken => new ShelfSortResult { ValidForShelf = false, Reason = ShelfSortResultReason.Unknown };
        public static ShelfSortResult TokenBlocked => new ShelfSortResult { ValidForShelf = true, Reason = ShelfSortResultReason.TokenBlocked };
        public static ShelfSortResult TokenSorted => new ShelfSortResult { ValidForShelf = true, Reason = ShelfSortResultReason.Sorted };

        private ShelfSortResult() { }

        public static bool operator <(ShelfSortResult a, ShelfSortResult b)
        {
            return a.CompareTo(b) < 0;
        }

        public static bool operator >(ShelfSortResult a, ShelfSortResult b)
        {
            return a.CompareTo(b) > 0;
        }

        public int CompareTo(ShelfSortResult other)
        {
            return GetOrdinal(this).CompareTo(GetOrdinal(other));
        }

        public override string ToString()
        {
            return this.Reason.ToString();
        }

        /// <summary>
        /// Gets the relative importance of this sort result.
        /// This is used when determining what result to send upstream from an area sort to a shelf sort.
        /// </summary>
        private static int GetOrdinal(ShelfSortResult result)
        {
            switch (result.Reason)
            {
                case ShelfSortResultReason.Unknown:
                    return 1;
                case ShelfSortResultReason.TokenBlocked:
                    return 2;
                case ShelfSortResultReason.Sorted:
                    return 3;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}