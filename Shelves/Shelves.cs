﻿using Roost;
using SecretHistories;
using SecretHistories.Commands.Encausting;
using SecretHistories.Entities;
using SecretHistories.Enums;
using SecretHistories.Services;
using SecretHistories.UI;
using Shelf;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Roost.Twins.Entities;
using SecretHistories.Spheres.Choreographers;
using SecretHistories.Entities.Verbs;
using SecretHistories.Assets.Scripts.Application.Infrastructure;
using SecretHistories.Commands;
using Shelf.Entities;
using SecretHistories.Fucine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShelvesVisibilitySettingSubscriber : ISettingSubscriber
{
    public void WhenSettingUpdated(object newValue)
    {
        bool hide = (float)newValue == 1;
        //Birdsong.Sing(Birdsong.Incr(), "Shelves Visibility setting changed! " + hide);
        //Birdsong.Sing(Birdsong.Incr(), "Scene name: " + SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name != "S0Master") return;
        this.UpdateShelvesVisibility(hide);
    }

    void UpdateShelvesVisibility(bool hide)
    {
        Birdsong.Sing(Birdsong.Incr(), "New value for the setting: " + hide);
        foreach(ShelfManifestation sm in Shelves.RegisteredShelfManifestations)
        {
            if (hide) sm.MakeInvisible();
            else sm.MakeVisible();
        }
    }
}


class Shelves
{
    public static SortedSet<ShelfManifestation> RegisteredShelfManifestations = new SortedSet<ShelfManifestation>();

    private static readonly TimeSpan AutoSortWait = TimeSpan.FromSeconds(1 / 5); // 5 times a second
    private static DateTime lastAutoSort = DateTime.MinValue;
    private static ExpiringSet<Token> blockedTokens = new ExpiringSet<Token>(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(2));

    public static void Initialise()
    {
        try
        {
            AtTimeOfPower.CompendiumLoad.Schedule(AttachSettingSubscribers, PatchType.Postfix);
            AtTimeOfPower.TabletopSceneInit.Schedule(ChangeStackButtonSprite, PatchType.Postfix);

            // Prefix patch to handle the spawning of Shelf manifestations (the base game's manifestation method cannot handle custom manifestations) 
            Machine.Patch(
                original: typeof(PrefabFactory).GetMethodInvariant(nameof(PrefabFactory.CreateManifestationPrefab)),
                prefix: typeof(Shelves).GetMethodInvariant(nameof(HandleZoneManifestationPrefab))
            );

            // Patch to make the serialiser play nice with Shelves (somehow? I don't remember why it works or how)
            Machine.Patch(
                original: typeof(EncaustablesSerializationBinder).GetConstructor(new Type[0]),
                postfix: typeof(Shelves).GetMethodInvariant(nameof(AddCommandToConstructor))
            );

            // Prefix patch to handle the stack action with our custom behaviour
            Machine.Patch(
                original: typeof(TabletopChoreographer).GetMethodInvariant(nameof(TabletopChoreographer.GroupAllStacks)),
                prefix: typeof(Shelves).GetMethodInvariant(nameof(GroupAllStacks), new Type[0])
            );

            // Postfix patch to handle spawning the proper shelves when a legacy is started
            // This should probably also be called when we restart a game? But...stuff is spawned when we restart a game, where does it come from? Here? Nope, no log printed
            Machine.Patch(
                original: typeof(CSTokenSetupChamberlain).GetMethodInvariant(nameof(CSTokenSetupChamberlain.GetDefaultSphereTokenCreationCommandsToEnactLegacy)),
                postfix: typeof(Shelves).GetMethodInvariant(nameof(GetDefaultSphereTokenCreationCommandsToEnactLegacy))
            );

            AtTimeOfPower.TabletopSceneInit.Schedule(SyncShelvesExistence, PatchType.Postfix);
            
            Roost.Vagabond.CommandLine.AddCommand("spawnshelf", SpawnShelf);

            ShelfCategories.Initialize();

            GameAPI.Initialise();
            GameAPI.GameStarted += OnGameStarted;
            GameAPI.GameEnded += OnGameEnded;

            ShelfUI.Initialise();

            TabletopObserver.TokenInvalidated += OnTokenInvalidated;
        }
        catch (Exception ex)
        {
            // Errors get eaten by the game on init.
            NoonUtility.LogWarning("Shelf initialization error: " + ex);
        }
    }

    public static void AttachSettingSubscribers()
    {
        var setting = Watchman.Get<Compendium>().GetEntityById<Setting>("hideshelves");
        if (setting != null) setting.AddSubscriber(new ShelvesVisibilitySettingSubscriber());
    }

    public static void ChangeStackButtonSprite()
    {
        var buttonGO = GameObject.Find("Button_Stack");
        buttonGO.GetComponent<Image>().sprite = ResourcesManager.GetSpriteForUI("shelves_stack_button");

        SpriteState ss = new()
        {
            pressedSprite = ResourcesManager.GetSpriteForUI("shelves_stack_button_active"),
            selectedSprite = ResourcesManager.GetSpriteForUI("shelves_stack_button_active"),
            highlightedSprite = ResourcesManager.GetSpriteForUI("shelves_stack_button_active")
        };
        buttonGO.GetComponent<Button>().spriteState = ss;
    }

    public static void SyncShelvesExistence()
    {
        var legacyId = Watchman.Get<Stable>().Protag().ActiveLegacyId;
        var tabletop = GameAPI.GetTabletopSphere();

        // This happens before the legacy is active.
        var targetLayout = $"legacy:{legacyId}";
        Birdsong.Sing(Birdsong.Incr(), $"TabletopInit for {legacyId}. Syncing Shelves and Dropzones...");

        // Update entity dropzone position
        var entityDropzone = tabletop.GetTokensWhere(token => token.Payload is Dropzone dz && dz.EntityId == "ElementStack").FirstOrDefault();
        var pos = ShelfConfig.GetDropzonePosition("ElementStack", targetLayout);
        Birdsong.Sing(Birdsong.Incr(), $"Updating the entity dropzone to the persisted pos {pos}, {entityDropzone}");
        if (entityDropzone && pos != Vector2.zero)
        {
            entityDropzone.gameObject.transform.localPosition = pos;
        }

        // Update zone dropzone position
        var situationDropzone = tabletop.GetTokensWhere(token => token.Payload is Dropzone dz && dz.EntityId == "Situation").FirstOrDefault();
        pos = ShelfConfig.GetDropzonePosition("Situation", targetLayout);
        Birdsong.Sing(Birdsong.Incr(), $"Updating the entity dropzone to the persisted pos {pos}, {situationDropzone}");
        if (situationDropzone && pos != Vector2.zero)
        {
            situationDropzone.gameObject.transform.localPosition = pos;
        }

        // Update the existence of shelves
        var persistedShelves = ShelfConfig.GetEnabledShelves(targetLayout);
        var existingShelves = tabletop.GetTokensWhere((Token token) => token.Payload is ShelfPayload);

        // Look for shelves to destroy
        Birdsong.Sing(Birdsong.Incr(), "Looking for existing shelves to destroy...");
        foreach(var existingShelve in existingShelves)
        {
            var shelfId = existingShelve.PayloadEntityId;
            Birdsong.Sing(Birdsong.Incr(), $"Found a shelf with id {shelfId}...");
            if (!persistedShelves.Any(key => key == shelfId))
            {
                Birdsong.Sing(Birdsong.Incr(), $"Not known by the config. Retiring shelf {shelfId}");
                existingShelve.Retire();
            }
        }

        // Look for shelves to spawn/move
        Birdsong.Sing(Birdsong.Incr(), "Looking for existing shelves to spawn or move...");
        foreach (var shelfId in persistedShelves)
        {
            var existingShelf = existingShelves.FirstOrDefault(token => token.PayloadEntityId == shelfId);
            Birdsong.Sing(Birdsong.Incr(), $"Found a shelf with id {shelfId} in the config...");
            if (!existingShelf)
            {
                Birdsong.Sing(Birdsong.Incr(), $"Shelf with id {shelfId} doesn't exist on the table. Spawning it...");
                var shelfCreationCommand = new ShelfCreationCommand(shelfId);
                var shelfPos = ShelfConfig.GetShelfPosition(shelfId, targetLayout);
                var shelfLocation = new TokenLocation(shelfPos, tabletop.GetAbsolutePath());
                var shelfTokenCreationCommand = new TokenCreationCommand(shelfCreationCommand, shelfLocation).WithAssertivePlacement();
                shelfTokenCreationCommand.Execute(Context.Unknown(), tabletop);
            }
            else
            {
                var shelfPos = ShelfConfig.GetShelfPosition(shelfId, targetLayout);
                existingShelf.gameObject.transform.localPosition = shelfPos;
                Birdsong.Sing(Birdsong.Incr(), $"Shelf with id {shelfId} found on the table. Updating its position to {shelfPos}");
            }
        }
    }

    // TODO: There's some confusion over who owns shelf existance.  This, or ShelfConfig.
    // ShelfConfig is the last word, but shelves get enabled by being dropped on the board, so this will
    // know first.
    public static IEnumerable<string> GetOpenShelves()
    {
        return RegisteredShelfManifestations.Select(sm => sm.Entity.Id);
    }

    public static bool IsTokenSortable(Token token)
    {
        if (token.Defunct)
        {
            return false;
        }

        if ((token.Payload is ElementStack) == false)
        {
            return false;
        }

        // We used to use token.CurrentState.IsPlayerDrivenMotion, but that gets stuck in weird states for brand new cards.
        // This is a much more reliable method, as we will not be on the table if dragging or itinerating.
        if (token.Sphere != GameAPI.GetTabletopSphere())
        {
            return false;
        }

        return true;
    }

    public static void DisableShelf(string shelfId)
    {
        ShelfConfig.DeleteShelf(shelfId);
        UpdateShelfExistence(shelfId, false);
    }

    private static void OnGameStarted(object sender, EventArgs e)
    {
        TabletopObserver.Enable();
        ShelfHeart.OnBeat += TryAutoSort;
    }

    private static void OnGameEnded(object sender, EventArgs e)
    {
        ShelfHeart.OnBeat -= TryAutoSort;

        TabletopObserver.Disable();
        FucineCache.Clear();
    }

    private static void OnTokenInvalidated(object sender, TabletopObserver.TokenEventArgs e)
    {
        var token = e.Token;
        if (token.Payload is ElementStack)
        {
            // Something interesting happened to this token, lets try sorting it again.
            blockedTokens.Remove(token);
            return;
        }

        var tabletop = GameAPI.GetTabletopSphere();

        if (token.Payload is ShelfPayload shelf)
        {
            if (token.Defunct)
            {
                // Note: This may be because the shelf was deleted, but the delete button deletes the shelf from the config on its own.
                // Do not delete the shelf from the config here, as this might be because of the game unloading.
                return;
            }

            var position = token.GetRectInOtherSphere(tabletop).center;

            ShelfConfig.SetShelfPosition(
                shelf.EntityId,
                position);

            // Shelf is no longer dragging, so it will no longer be blocking tokens.
            // Clear out the list so we can immediatly retry.
            blockedTokens.Clear();
        }
        else if (token.Payload is Dropzone dropzone)
        {
            var position = token.GetRectInOtherSphere(tabletop).center;

            ShelfConfig.SetDropzonePosition(
                dropzone.EntityId,
                position);
        }
    }

    private static void UpdateShelfExistence(string shelfId, bool enabled)
    {
        var shelfManifestation = Shelves.RegisteredShelfManifestations.FirstOrDefault(shelf => shelf.Entity.Id == shelfId);

        if (shelfManifestation == null && enabled)
        {
            string[] args = { shelfId };
            SpawnShelf(args);
        }

        if (shelfManifestation != null && !enabled)
        {
            shelfManifestation.Token.Retire(RetirementVFX.CardTaken);
        }
    }

    private static void SpawnShelf(string[] args)
    {
        if (args.Length == 0)
        {
            Birdsong.Sing(Birdsong.Incr(), "Usage: /spawnshelf [id of shelf]");
            return;
        }

        var tabletop = GameAPI.GetTabletopSphere();

        var shelfId = args[0];

        var tabletopSphere = GameAPI.GetTabletopSphere();

        // TODO: Try using WithAssertivePlacement on TokenCreationCommand instead of reimplementing everything.
        // Old method: Use creation command.
        // var zoneTokenCreationCommand = new TokenCreationCommand(zoneCreationCommand, zoneLocation).WithDestination(zoneLocation, quickDuration);
        // zoneTokenCreationCommand.Execute(Context.Unknown(), tabletopSphere);

        // Spawn the shelf immediately and manually so that we do not conflict with dropzones and verbs.
        // This is more or less a base game bug.  We can drop shelves on these constructs because they have
        // a different OccupySpaceAs, but the itineraries do not check for that and will wobble around.

        var zoneCreationCommand = new ShelfCreationCommand(shelfId);

        var shelf = zoneCreationCommand.Execute(Context.Unknown());
        if (!shelf.IsValid())
        {
            return;
        }

        var previousLocation = ShelfConfig.GetShelfPosition(shelfId);
        var zoneLocation = new TokenLocation(previousLocation, tabletopSphere.GetAbsolutePath());

        var token = Watchman.Get<PrefabFactory>().CreateLocally<Token>(tabletop.GetRectTransform());
        token.SetPayload(shelf);

        token.TokenRectTransform.localPosition = zoneLocation.LocalPosition;
        tabletop.AcceptToken(token, Context.Unknown());
    }

    private static bool HandleZoneManifestationPrefab(Type manifestationType, Transform parent, ref IManifestation __result)
    {
        if (manifestationType != typeof(ShelfManifestation)) return true;

        GameObject go = new GameObject();
        go.transform.SetParent(parent);
        ShelfManifestation zm = go.AddComponent<ShelfManifestation>();

        __result = zm;
        return false;
    }

    private static void AddCommandToConstructor(EncaustablesSerializationBinder __instance)
    {
        HashSet<Type> l = Machine.GetFieldInvariant(typeof(EncaustablesSerializationBinder), "encaustmentTypes").GetValue(__instance) as HashSet<Type>;
        l.Add(typeof(ShelfCreationCommand));
    }

    private static void GetDefaultSphereTokenCreationCommandsToEnactLegacy(Legacy forLegacy, CSTokenSetupChamberlain __instance, ref List<TokenCreationCommand> __result)
    {
        var tabletop = GameAPI.GetTabletopSphere();

        // This happens before the legacy is active.
        var targetLayout = $"legacy:{forLegacy.Id}";
        Birdsong.Sing(Birdsong.Incr(), $"Updating dropzones for {targetLayout}");

        var longDuration = Watchman.Get<Compendium>().GetSingleEntity<Dictum>().DefaultLongTravelDuration;

        var elementStackDropzoneCommand = __result.FirstOrDefault(x => x.Payload is DropzoneCreationCommand dz && dz.EntityId == "ElementStack");
        if (elementStackDropzoneCommand != null)
        {
            var pos = ShelfConfig.GetDropzonePosition("ElementStack", targetLayout);
            if (pos != Vector2.zero)
            {
                var location = new TokenLocation(pos, elementStackDropzoneCommand.Location.AtSpherePath);
                elementStackDropzoneCommand.WithDestination(location, longDuration).WithAssertivePlacement();
            }
        }

        var situationDropzoneCommand = __result.FirstOrDefault(x => x.Payload is DropzoneCreationCommand dz && dz.EntityId == "Situation");
        if (situationDropzoneCommand != null)
        {
            var pos = ShelfConfig.GetDropzonePosition("Situation", targetLayout);
            if (pos != Vector2.zero)
            {
                var location = new TokenLocation(pos, situationDropzoneCommand.Location.AtSpherePath);
                situationDropzoneCommand.WithDestination(location, longDuration).WithAssertivePlacement();
            }
        }

/*        var spawnShelves = ShelfConfig.GetEnabledShelves(targetLayout);
        var placementDelay = 0f;
        foreach (var shelfId in spawnShelves)
        {
            var zoneCreationCommand = new ShelfCreationCommand(shelfId);
            var pos = ShelfConfig.GetShelfPosition(shelfId, targetLayout);
            // var startingLocation = new TokenLocation(0.0f, pos.y > 0 ? 2000f : -2000f, 0.0f, Watchman.Get<HornedAxe>().GetDefaultSpherePathForUnknownToken());
            var zoneLocation = new TokenLocation(pos, tabletop.GetAbsolutePath());
            // This would have made them animate in, but it has a few issues:
            // - Shelves can save in the restart.json while animating, bricking their positions in the config
            // - On restart, shelf manifestatiosn bafflingly have a 25 unit offset in the z position.
            // var zoneTokenCreationCommand = new TokenCreationCommand(zoneCreationCommand, startingLocation).WithDestination(zoneLocation, longDuration + placementDelay).WithAssertivePlacement();
            var zoneTokenCreationCommand = new TokenCreationCommand(zoneCreationCommand, zoneLocation).WithAssertivePlacement();
            placementDelay += 0.05f;
            __result.Add(zoneTokenCreationCommand);
        }*/
    }

    private static void SetShelfCachedValues(ShelfEntity se, Action<string, FucineExp<int>> op)
    {
        foreach (Dictionary<string, FucineExp<int>> keyValuePairs in se.RootCache)
        {
            foreach (string variableName in keyValuePairs.Keys)
            {
                op(variableName, keyValuePairs[variableName]);
            }
        }
    }

    private static void LoadShelfCachedValues(ShelfEntity se)
    {
        SetShelfCachedValues(se, (variableName, exp) => FucineRoot.Get().SetMutation(variableName, exp.value, false));
    }

    private static void ClearShelfCachedValues(ShelfEntity se)
    {
        SetShelfCachedValues(se, (variableName, exp) => FucineRoot.Get().SetMutation(variableName, 0, false));
    }

    private static bool AutoShortShelvesEnabled()
    {
        object obj = Watchman.Get<Config>().GetConfigValue("autoorganizeshelves");
        return obj != null && obj.ToString().Equals("1");
    }

    private static void TryAutoSort(object sender, EventArgs e)
    {
        if (DateTime.Now - lastAutoSort < AutoSortWait)
        {
            return;
        }

        if (!AutoShortShelvesEnabled())
        {
            return;
        }

        if (Watchman.Get<Meniscate>().GetCurrentlyDraggedToken()?.Payload is ShelfPayload)
        {
            // Shelf is dragging, which means lots of cards are going to be blocked.
            // Don't try to sort.
            return;
        }

        lastAutoSort = DateTime.Now;

        var tabletop = GameAPI.GetTabletopSphere();

        var tokensToSort = TabletopObserver.GetDirtyTokens().Where(x => !TabletopObserver.IsExempted(x) && !blockedTokens.Contains(x)).ToList();
        if (tokensToSort.Count > 0)
        {
            MoveStacksToShelves(tokensToSort);
        }
    }

    public static bool GroupAllStacks()
    {
        TabletopObserver.ClearExemptedTokens();
        blockedTokens.Clear();

        var tokensToSort = TabletopObserver.GetDirtyTokens();
        MoveStacksToShelves(tokensToSort);

        return false;
    }

    public static void MoveStacksToShelves(IEnumerable<Token> tokensToSort)
    {
        var remainingTokens = new HashSet<Token>();
        foreach (var token in tokensToSort)
        {
            if (IsTokenSortable(token))
            {
                remainingTokens.Add(token);
            }
            else
            {
                blockedTokens.Remove(token);
                TabletopObserver.MarkSorted(token);
            }
        }

        if (remainingTokens.Count == 0)
        {
            return;
        }

        // This is a reimplementation of the original GroupAllStacks method.
        // We cannot let it group everything everywhere, as that will group cards that we have exempted.
        // This is fine for push button sort, but not for autosort.
        // This has the quirk that we might have invalidated one card of a group but not the rest, so we need to determine all possible grouping cards outside those
        // we have invalidated by checking the signatures we have.
        var signaturesToSort = new HashSet<string>(remainingTokens.Select(t => t.Payload.GetSignature()));
        var groupTargets =
            from token in GameAPI.GetTabletopSphere().GetElementTokens()
                // Don't touch exempted tokens.  Note that this is for both stacking into them,
                // and for stacking them into something else.
                // We don't want the latter, but its not clear if we want the former.
                // Note that this is effectively only for auto-sort, as a non-autosort operation will not have any exemptions.
            where !TabletopObserver.IsExempted(token)
            let signature = token.Payload.GetSignature()
            where signaturesToSort.Contains(signature)
            group token by signature into g
            select g.OrderByDescending(e => e.Payload.Quantity);

        foreach (var source in groupTargets.ToList())
        {
            var token = source.First();
            foreach (var incomingToken in source.Skip(1))
            {
                if (token.Payload.CanMergeWith(incomingToken.Payload))
                {
                    token.Payload.InteractWithIncoming(incomingToken);

                    // Note: 'token' might not be in our remainingTokens list if it was not invalidated.
                    // This is fine, as it means whever the token was is already in its happy place.

                    // If incommingToken is now defunct, it was merged
                    if (incomingToken.Defunct)
                    {
                        remainingTokens.Remove(incomingToken);
                        blockedTokens.Remove(incomingToken);
                        TabletopObserver.MarkSorted(incomingToken);
                    }
                }
            }
        }

        var tokensWithoutShelves = new HashSet<Token>(remainingTokens);

        // Higher priority shelves are sorted first.
        var manifestationsInOrder = RegisteredShelfManifestations.Reverse();
        foreach (var sm in manifestationsInOrder)
        {
            LoadShelfCachedValues(sm.Entity);

            try
            {
                foreach (var token in sm.Entity.FilterTokens(remainingTokens).ToArray())
                {
                    var result = sm.ArrangeToken(token);

                    if (result.ValidForShelf)
                    {
                        tokensWithoutShelves.Remove(token);
                    }

                    if (result.Sorted)
                    {
                        remainingTokens.Remove(token);
                        blockedTokens.Remove(token);
                        TabletopObserver.MarkSorted(token);
                    }
                    else if (result.Blocked)
                    {
                        blockedTokens.Add(token);
                    }
                }
            }
            catch (ShelfExpressionException e)
            {
                Shelves.DisableShelf(e.ShelfId);
                Watchman.Get<Notifier>().ShowNotificationWindow("Fucine Error", e.Message);
                NoonUtility.LogWarning(e.Message);
            }
            finally
            {
                ClearShelfCachedValues(sm.Entity);
            }
        }

        foreach (var token in tokensWithoutShelves)
        {
            // These tokens don't belong to any shelf.  Mark them as sorted so we can ignore them next time.
            blockedTokens.Remove(token);
            TabletopObserver.MarkSorted(token);
        }
    }
}
