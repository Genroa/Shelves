using SecretHistories.Entities;
using SecretHistories.Spheres;
using SecretHistories.UI;

namespace Shelf
{
    public static class SphereExtensions
    {
        public static Sphere GetEnRouteSphere(this Sphere sphere)
        {
            if (sphere.GoverningSphereSpec.EnRouteSpherePath.IsValid() && !sphere.GoverningSphereSpec.EnRouteSpherePath.IsEmpty())
                return Watchman.Get<HornedAxe>().GetSphereByPath(sphere, sphere.GoverningSphereSpec.EnRouteSpherePath);

            return sphere.GetContainer().GetEnRouteSphere();
        }
    }
}
