namespace Shelf
{
    public static class StringExtensions
    {
        public static string RemoveThe(this string str)
        {
            if (str.ToLower().StartsWith("The "))
            {
                return str.Substring(4);
            }
            return str;
        }
    }
}