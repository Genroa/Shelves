using System.Collections.Generic;
using SecretHistories.Fucine;
using SecretHistories.Events;
using SecretHistories.UI;
using SecretHistories.Enums;
using System;
using Roost;
using SecretHistories.Spheres;
using System.Linq;
using Shelf.Entities;

namespace Shelf
{
    public class TabletopObserver : ISphereEventSubscriber
    {
        /// <summary>
        /// A set of tokens that have changed in some way, and need to be sorted.
        /// </summary>
        private static readonly HashSet<Token> dirtyTokens = new();

        /// <summary>
        /// A set of tokens that have been exempted from sorting, because the player has chosen to put them somewhere.
        /// </summary>
        private static readonly HashSet<string> exemptedTokenIds = new();

        private static bool enabled = false;

        private static TabletopObserver instance = new();

        static TabletopObserver()
        {
            try
            {
                Machine.Patch(
                    original: typeof(TabletopSphere).GetMethodInvariant(nameof(TabletopSphere.AcceptToken)),
                    postfix: typeof(TabletopObserver).GetMethodInvariant(nameof(TabletopObserver.AcceptTabletopToken))
                );
            }
            catch (Exception ex)
            {
                NoonUtility.LogWarning("Failed to patch TabletopSphere.AcceptToken");
                NoonUtility.LogException(ex);
            }
        }

        public static event EventHandler<TokenEventArgs> TokenInvalidated;

        public static IReadOnlyCollection<string> ExemptTokenIds
        {
            get
            {
                return exemptedTokenIds;
            }
        }

        public static void Enable()
        {
            if (enabled)
            {
                // Restart
                Disable();
            }

            enabled = true;

            exemptedTokenIds.Clear();

            var tabletopSphere = GameAPI.GetTabletopSphere();
            foreach (var token in tabletopSphere.GetElementTokens())
            {
                dirtyTokens.Add(token);
            }

            tabletopSphere.Subscribe(instance);
        }

        public static void Disable()
        {
            if (!enabled)
            {
                return;
            }

            enabled = false;

            GameAPI.GetTabletopSphere().Unsubscribe(instance);

            // Clear on disable so we don't hold references to dead things.
            dirtyTokens.Clear();
            exemptedTokenIds.Clear();
        }

        public static IReadOnlyCollection<Token> GetDirtyTokens()
        {
            return dirtyTokens.ToArray();
        }

        public static void ClearExemptedTokens()
        {
            exemptedTokenIds.Clear();
        }

        public static bool IsExempted(Token token)
        {
            return exemptedTokenIds.Contains(token.PayloadId);
        }

        public static void InvalidateAll()
        {
            dirtyTokens.Clear();
            foreach (var token in GameAPI.GetTabletopSphere().GetElementTokens())
            {
                dirtyTokens.Add(token);
            }
        }

        public static void MarkSorted(Token token)
        {
            if (token == null)
            {
                return;
            }

            dirtyTokens.Remove(token);
            exemptedTokenIds.Remove(token.PayloadId);
        }

        void ISphereEventSubscriber.OnTokensChangedForSphere(SphereContentsChangedEventArgs args)
        {
            if (args.Sphere != GameAPI.GetTabletopSphere())
            {
                // Don't care about inner spheres, like the dragging card sphere.
                return;
            }

            TokenInvalidated?.Invoke(this, new TokenEventArgs(args.Token));

            if (args.Token.Payload is ShelfPayload shelfPayload)
            {
                InvalidateAll();
                return;
            }

            switch (args.Change)
            {
                case SphereContentChange.TokenAdded:
                    if (args.Token.Payload is ElementStack)
                    {
                        ElementStackInvalidated(args.Token);
                    }
                    break;
                case SphereContentChange.TokenChanged:
                    // This is raised for ElementStack.OnChange if its a Fundimental change or if its NotifySphere is true
                    // In practice, tokene element changes are Fundimental changes, and SetMutation is the only change to set NotifySphere.
                    // At one point, we listened to each token's OnChange directly, but that has a risk of leaks and was causing trouble.
                    if (args.Token.Payload is ElementStack)
                    {
                        ElementStackInvalidated(args.Token);
                    }
                    break;
                case SphereContentChange.TokenRemoved:
                    if (args.Token.Defunct)
                    {
                        // Only if the token went defunct should we remove it from exemptions.
                        // We want to remember its exemption through other spheres.
                        exemptedTokenIds.Remove(args.Token.PayloadId);
                    }
                    dirtyTokens.Remove(args.Token);
                    break;
            }
        }

        void ISphereEventSubscriber.OnSphereChanged(SphereChangedArgs args) { }
        void ISphereEventSubscriber.OnTokenInteractionInSphere(TokenInteractionEventArgs args)
        {
            // Note: We used to listen to DragBegin here for exemptions, but that happens
            // when a player is dragging a token to another sphere, which should NOT exempt it.
            // Additionally, we cannot listen for DragEnd, as that:
            // - is broadcasted to the EnRouteSphere
            // - is called on the EnRouteSphere before the token actually resolves what sphere it is going to
            // - will not resolve the sphere until a further Itinerary completes.
            // All in all, it is simpler to patch TabletopSphere.AcceptToken, which we do.
        }

        private static void AcceptTabletopToken(Token token, Context context)
        {
            if (token == null)
            {
                return;
            }

            if (context.actionSource == Context.ActionSource.PlayerDrag)
            {
                // Player dropped a token at a specific spot
                exemptedTokenIds.Add(token.PayloadId);
                // Force this to be dirty, so it sorts when the stack button is pressed.
                // Note: This should be happening anyway from the token added event... it isnt?
                dirtyTokens.Add(token);
            }
        }

        private void ElementStackInvalidated(Token token)
        {
            if (token == null)
            {
                return;
            }

            dirtyTokens.Add(token);
        }

        public class TokenEventArgs : EventArgs
        {
            public TokenEventArgs(Token token)
            {
                this.Token = token;
            }

            public Token Token { get; }
        }
    }
}
