namespace Shelf
{
    public interface IShelvesView
    {
        void Update();
        void OnDestroy();
    }
}