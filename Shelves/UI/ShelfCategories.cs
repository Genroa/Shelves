using System;
using System.Collections.Generic;
using System.Linq;
using AutoccultistNS;
using Roost;
using SecretHistories.Entities;
using SecretHistories.UI;
using UnityEngine;
using Shelf.Entities;

namespace Shelf
{
    public static class ShelfCategories
    {
        private static HashSet<string> ExcludeLegacies = new()
        {
            "lbarsacrossthesun",
            "exile",
            "exile.obscurityvictorya",
            "exile.obscurityvictoryb",
            "exile.obscurityvictoryc",
            "exile.obscurityvictoryafoeslain",
            "exile.obscurityvictorybfoeslain",
            "exile.obscurityvictorycfoeslain",
            "exile.foecaughtup",
            "exile.ascensioncolonel",
            "exile.ascensionlionsmith",
            "exile.ascensionwolf",
            "exile.victoryvelvet",
            "dancer",
            "ghoul",
            "priest",
            "aspirant",
            "detective",
            "brightyoungthing",
            "physician",
            "detectivepostpromotion",
            "apostleforge",
            "apostlegrail",
            "apostlelantern",
            "apostleforgewithrisen",
            "apostlegrailwithrisen",
            "apostlelanternwithrisen",
            "apostlelantern",
            "auctioneer",
            "survivor",
            "cousin"
        };

        public static IReadOnlyList<IShelfCategory> CoreCategories = new List<IShelfCategory>(); // empty and replaced during BuildCoreCategories (names are fetched at runtime because they're taken from culture loc strings)
        public static IReadOnlyList<IShelfCategory> Categories = new List<IShelfCategory>();

        public static void Initialize()
        {
            AtTimeOfPower.TabletopSceneInit.Schedule(GenerateCategories, PatchType.Postfix);
        }

        public static IEnumerable<IShelfCategory> GetVisibleCategories()
        {
            return from category in Categories
                   where IsCategoryVisible(category)
                   select category;
        }

        public static IShelfCategory GetCategory(string categoryId)
        {
            var category = Categories.FirstOrDefault(x => x.Id == categoryId);
            if (category != null)
            {
                return category;
            }

            return CoreCategories.Last();
        }

        public static string GetNextCategoryId(string categoryId)
        {
            var index = Categories.FindIndex(x => x.Id == categoryId);
            var origin = index;
            do
            {
                index = (index + 1) % Categories.Count;
            }
            while (index != origin && !IsCategoryVisible(Categories[index]));

            return Categories[index].Id;
        }

        public static string GetPreviousCategoryId(string categoryId)
        {
            var index = Categories.FindIndex(x => x.Id == categoryId);
            var origin = index;
            do
            {
                index = (index + Categories.Count - 1) % Categories.Count;
            }
            while (index != origin && !IsCategoryVisible(Categories[index]));
            return Categories[index].Id;
        }

        private static void BuildCoreCategories()
        {
            ILocStringProvider provider = Watchman.Get<ILocStringProvider>();
            CoreCategories = new List<IShelfCategory>()
            { 
                new CurrentLegacyShelfCategory(),
                new ShelfCategory("abilities", "ability", provider.Get("UI_SHELVES_CATEGORY_ABILITIES")),
                new ShelfCategory("lore", "lore", provider.Get("UI_SHELVES_CATEGORY_LORE")),
                new ShelfCategory("followers", "follower", provider.Get("UI_SHELVES_CATEGORY_FOLLOWERS")),
                new ShelfCategory("tools", "tool", provider.Get("UI_SHELVES_CATEGORY_TOOLS")),
                new ShelfCategory("texts", "text", provider.Get("UI_SHELVES_CATEGORY_TEXTS")),
                new ShelfCategory("vaults", "vault", provider.Get("UI_SHELVES_CATEGORY_VAULTS")),
                new ShelfCategory("rites", "ritual", provider.Get("UI_SHELVES_CATEGORY_RITES")),
                new ShelfCategory("influences", "influence", provider.Get("UI_SHELVES_CATEGORY_INFLUENCES")),
                new ShelfCategory("jobs", "job", provider.Get("UI_SHELVES_CATEGORY_JOBS")),
                new ShelfCategory("ways", "way", provider.Get("UI_SHELVES_CATEGORY_DREAM_WAYS")),
                new ShelfCategory("reputation", "reputation", provider.Get("UI_SHELVES_CATEGORY_REPUTATION")),
                new ShelfCategory("ingredients", "ingredient", provider.Get("UI_SHELVES_CATEGORY_INGREDIENTS")),

                // Legacy categories
                new ShelfCategory("apostles", "apostleforge", provider.Get("UI_SHELVES_CATEGORY_APOSTLES"), 1),
                new ShelfCategory("dancer", "dancer", provider.Get("UI_SHELVES_CATEGORY_DANCER"), 1),
                new ShelfCategory("exile", "exile", provider.Get("UI_SHELVES_CATEGORY_EXILE"), 1),
                new ShelfCategory("ghoul", "ghoul", provider.Get("UI_SHELVES_CATEGORY_GHOUL"), 1),
                new ShelfCategory("priest", "priest", provider.Get("UI_SHELVES_CATEGORY_PRIEST"), 1),

                // Legacy (heh) category.  This is deprecated in favor of the auto-generating mod categories.
                // Note: Shelves that used this will have to be manually updated to use the new category.
                // new ShelfCategory("mariner", "ghoul", "Mariner"),

                // Default
                new OtherCategory()
            };
        }

        private static void GenerateCategories()
        {
            BuildCoreCategories();
            var categories = new List<IShelfCategory>();
            Categories = categories;

            var legacies = from legacy in Watchman.Get<Compendium>().GetEntitiesAsList<Legacy>()
                           where !ExcludeLegacies.Contains(legacy.Id)
                           group legacy by legacy.Label into byLabel
                           select byLabel;

            var legacyCategories = new List<IShelfCategory>();
            foreach (var legacy in legacies)
            {
                legacyCategories.Add(new LegacyCategory($"legacy:{legacy.Key}", legacy.Select(x => x.Id), ResourcesManager.GetSpriteForLegacy(legacy.First().Image), legacy.Key, 10));
            }

            var sorted = from category in CoreCategories.Concat(legacyCategories)
                         orderby category.Weight, category.Name.RemoveThe()
                         select category;
            categories.AddRange(sorted);
        }

        private static bool IsCategoryVisible(IShelfCategory category)
        {
            return category.GetShelves().Any();
        }

        public interface IShelfCategory
        {
            string Id { get; }
            string Name { get; }
            Sprite Sprite { get; }

            int Weight { get; }

            IEnumerable<ShelfEntity> GetShelves();
        }

        private class ShelfCategory : IShelfCategory
        {
            protected Lazy<Sprite> sprite;

            public string Id { get; }
            public string Name { get; }
            public Sprite Sprite => sprite.Value;

            public virtual int Weight { get; }

            public ShelfCategory(string id, string sprite, string name, int weight = 0)
            {
                this.Id = id;
                this.Name = name;
                this.Weight = weight;

                // because of our jank lookup mechanism and the impossibility of finding these by name, we need to delay
                // the lookup until a time where it is feasible that the icon would have already been loaded.
                this.sprite = new Lazy<Sprite>(() => ResourceHack.FindSprite(sprite));
            }

            public ShelfCategory(string id, Sprite sprite, string name, int weight = 0)
            {
                this.Id = id;
                this.Name = name;
                this.sprite = new Lazy<Sprite>(() => sprite);
                this.Weight = weight;
            }

            public virtual IEnumerable<ShelfEntity> GetShelves()
            {
                var entities = Watchman.Get<Compendium>().GetEntitiesAsList<ShelfEntity>();
                return entities.Where(x => x.Category == Id);
            }
        }

        private class OtherCategory : ShelfCategory
        {
            public override int Weight => int.MaxValue;

            public OtherCategory()
                : base(string.Empty, "memory", Watchman.Get<ILocStringProvider>().Get("UI_SHELVES_CATEGORY_OTHER"))
            {
            }

            public override IEnumerable<ShelfEntity> GetShelves()
            {
                var knownCategories = new HashSet<string>(Categories.Select(x => x.Id));
                var entities = Watchman.Get<Compendium>().GetEntitiesAsList<ShelfEntity>();
                return entities.Where(x => string.IsNullOrEmpty(x.Category) || !knownCategories.Contains(x.Category));
            }
        }

        private class LegacyCategory : ShelfCategory
        {
            public List<string> LegacyIds { get; }

            public LegacyCategory(string id, IEnumerable<string> legacyIds, Sprite sprite, string name, int weight = 0)
                : base(id, sprite, name, weight)
            {
                this.LegacyIds = legacyIds.ToList();
            }

            public override IEnumerable<ShelfEntity> GetShelves()
            {
                var entities = Watchman.Get<Compendium>().GetEntitiesAsList<ShelfEntity>();
                return entities.Where(x => x.Category == this.Id || x.Legacies.Any(y => LegacyIds.Contains(y)));
            }
        }

        private class CurrentLegacyShelfCategory : ShelfCategory
        {
            public CurrentLegacyShelfCategory()
                : base("meta:current_legacy", "empty_bg", "Current Legacy", -10)
            {
                GameAPI.GameLoadRequested += OnGameLoaded;
            }

            public override IEnumerable<ShelfEntity> GetShelves()
            {
                var currentLegacy = Watchman.Get<Stable>().Protag().ActiveLegacyId;
                var entities = Watchman.Get<Compendium>().GetEntitiesAsList<ShelfEntity>();
                return entities.Where(x => x.Legacies.Contains(currentLegacy));
            }

            private void OnGameLoaded(object sender, EventArgs e)
            {
                var currentLegacy = Watchman.Get<Stable>().Protag().ActiveLegacyId;
                var legacy = Watchman.Get<Compendium>().GetEntityById<Legacy>(currentLegacy);

                if (legacy == null)
                {
                    this.sprite = new Lazy<Sprite>(() => ResourcesManager.GetSpriteForLegacy("x"));
                    return;
                }

                this.sprite = new Lazy<Sprite>(() => ResourcesManager.GetSpriteForLegacy(legacy.Image));
            }
        }
    }
}
