using Roost;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using Roost.Piebald;

namespace Shelf
{
    public static class ShelfUI
    {


        private static ShelvesWindow shelvesDrawerWindow;
        private static GameObject stackButton;

        private static bool isOpen = false;

        public static bool IsOpen
        {
            get
            {
                return isOpen;
            }
        }

        public static event EventHandler UIToggled;

        public static void Initialise()
        {
            AtTimeOfPower.TabletopSceneInit.Schedule(BuildUI, PatchType.Postfix);
        }

        private static void BuildUI()
        {
            if (shelvesDrawerWindow != null)
            {

                shelvesDrawerWindow.Closed -= OnWindowClosed;
                shelvesDrawerWindow.Retire();
                shelvesDrawerWindow = null;
            }

            stackButton = GameObject.Find("Button_Stack");
            if (stackButton == null)
            {
                return;
            }

            // Unfortunately, the tokens expect pointer events to come from the tabletop camera,
            // so the window also has to be on the tabletop.
            shelvesDrawerWindow = WindowFactory.CreateWindow<ShelvesWindow>("Window_shelves");

            shelvesDrawerWindow.Closed += OnWindowClosed;

            new UIGameObjectWidget(stackButton).OnPointerClick(e =>
            {
                if (e.button != PointerEventData.InputButton.Right)
                {
                    return;
                }

                ToggleShelfsDrawer();
            });
        }

        private static void OnWindowClosed(object sender, EventArgs e)
        {
            if (!isOpen)
            {
                return;
            }

            isOpen = false;
            UIToggled?.Invoke(null, EventArgs.Empty);
        }

        private static void ToggleShelfsDrawer()
        {
            isOpen = !isOpen;

            if (!isOpen)
            {
                shelvesDrawerWindow.Close();
            }
            else
            {
                // Note: This position seems to be totally out of whack with the actual position of the button.
                // Still, its appearing in a good place, so ignoring that for now.
                shelvesDrawerWindow.OpenAt((Vector2)stackButton.transform.position + new Vector2(0, 600));
            }

            UIToggled?.Invoke(null, EventArgs.Empty);
        }
    }
}
