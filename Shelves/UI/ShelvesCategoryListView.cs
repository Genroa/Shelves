using Roost.Piebald;
using SecretHistories.UI;
using UnityEngine;

namespace Shelf
{
    class ShelvesCategoryListView : IShelvesView
    {
        private ShelvesWindow window;
        private WidgetMountPoint contentMountPoint;

        public ShelvesCategoryListView(ShelvesWindow window, WidgetMountPoint icon, WidgetMountPoint contentMountPoint, WidgetMountPoint footerMountPoint)
        {
            this.window = window;
            this.contentMountPoint = contentMountPoint;

            icon.Clear();
            icon.AddImage("CategoryListIcon")
                .CenterImage()
                .SetSprite("aspect:memory");

            this.RebuildContent();
        }

        public void OnDestroy()
        {
        }

        public void Update()
        {
        }

        private void RebuildContent()
        {
            this.contentMountPoint.Clear();

            this.contentMountPoint.AddVerticalLayoutGroup("Content")
                .SetSpreadChildrenHorizontally()
                .AddContent(mountPoint =>
                {
                    mountPoint.AddText("CategoryTitle")
                        .SetFontSize(36)
                        .SetHorizontalAlignment(TMPro.HorizontalAlignmentOptions.Center)
                        .SetUIText("UI_SHELVES_CATEGORIES");

                    mountPoint.AddLayoutItem("Spacing")
                        .SetPreferredHeight(10);

                    mountPoint.AddScrollRegion("ScrollRegion")
                        .SetExpandWidth()
                        .SetExpandHeight()
                        .SetVertical()
                        .AddContent(this.BuildCategoryList);
                });
        }

        private void BuildCategoryList(WidgetMountPoint mountPoint)
        {
            var list = (WidgetMountPoint)mountPoint.AddVerticalLayoutGroup("Shelves")
                .SetExpandWidth()
                .SetFitContentHeight()
                .SetSpreadChildrenHorizontally()
                .SetPadding(0, 10)
                .SetSpacing(30);

            foreach (var partition in ShelfCategories.GetVisibleCategories().Partition(3))
            {
                list.AddHorizontalLayoutGroup()
                    .SetExpandWidth()
                    .SetFitContentHeight()
                    .SetSpacing(30)
                    .SetChildAlignment(TextAnchor.UpperCenter)
                    .AddContent(mountPoint =>
                    {
                        foreach (var shelf in partition)
                        {
                            BuildCategoryItem(shelf, mountPoint);
                        }
                    });
            }
        }

        private void BuildCategoryItem(ShelfCategories.IShelfCategory category, WidgetMountPoint mountPoint)
        {
            mountPoint.AddVerticalLayoutGroup($"Category_{category.Name}")
                .OnPointerClick((e) => this.window.ShowCategory(category.Id))
                .SetSpacing(5)
                .AddContent(mountPoint =>
                {
                    mountPoint.AddLayoutItem()
                        .SetPreferredWidth(100)
                        .SetPreferredHeight(100)
                        .WithBehavior<GlowOnHover>()
                        .AddContent(mountPoint =>
                        {
                            mountPoint.AddImage()
                                .SetPreferredWidth(100)
                                .SetPreferredHeight(100)
                                .CenterImage()
                                .SetSprite(category.Sprite);
                        });

                    mountPoint.AddText()
                        .SetFontSize(20)
                        .SetPreferredWidth(100)
                        .SetHorizontalAlignment(TMPro.HorizontalAlignmentOptions.Center)
                        .SetVerticalAlignment(TMPro.VerticalAlignmentOptions.Middle)
                        .SetText(category.Name);
                });
        }
    }
}
