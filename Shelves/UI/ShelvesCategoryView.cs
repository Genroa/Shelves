using System;
using System.Collections.Generic;
using System.Linq;
using Roost.Piebald;
using SecretHistories.Services;
using SecretHistories.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using Shelf.Entities;

namespace Shelf
{
    class ShelvesCategoryView : IShelvesView
    {
        private Token draggedShelf;

        private Dictionary<string, ImageWidget> shelfIcons = new();

        private ShelvesWindow window;
        private string categoryId;
        private WidgetMountPoint contentMountPoint;
        private WidgetMountPoint footerMountPoint;

        public ShelvesCategoryView(ShelvesWindow window, string categoryId, WidgetMountPoint icon, WidgetMountPoint contentMountPoint, WidgetMountPoint footerMountPoint)
        {
            this.window = window;
            this.categoryId = categoryId;
            this.contentMountPoint = contentMountPoint;
            this.footerMountPoint = footerMountPoint;

            icon.Clear();
            icon.AddImage("CategoryListIcon")
                .CenterImage()
                .SetSprite(ShelfCategories.GetCategory(this.categoryId).Sprite);

            this.RebuildContent();
        }

        public void Update()
        {
            foreach (var pair in this.shelfIcons)
            {
                pair.Value.SetColor(Shelves.GetOpenShelves().Contains(pair.Key) ? new Color(0.3f, 0.3f, 0.3f, 1) : Color.white);
            }
        }

        public void OnDestroy()
        {
            if (this.draggedShelf != null)
            {
                this.draggedShelf.Retire();
                this.draggedShelf = null;
            }
        }

        private void RebuildContent()
        {
            this.shelfIcons.Clear();

            this.contentMountPoint.Clear();
            this.contentMountPoint.AddVerticalLayoutGroup("Content")
                .SetSpreadChildrenHorizontally()
                .AddContent(mountPoint =>
                {
                    mountPoint.AddText("CategoryTitle")
                        .SetFontSize(36)
                        .SetHorizontalAlignment(TMPro.HorizontalAlignmentOptions.Center)
                        .SetText(ShelfCategories.GetCategory(this.categoryId).Name);

                    mountPoint.AddLayoutItem("Spacing")
                        .SetPreferredHeight(10);

                    mountPoint.AddText("Instructions")
                        .SetFontSize(24)
                        .SetHorizontalAlignment(TMPro.HorizontalAlignmentOptions.Center)
                        .SetUIText("UI_SHELVES_DRAG_INSTRUCTIONS");

                    mountPoint.AddScrollRegion("ScrollRegion")
                        .SetExpandWidth()
                        .SetExpandHeight()
                        .SetVertical()
                        .AddContent(this.BuildShelfList);
                });

            this.footerMountPoint.Clear();
            this.footerMountPoint.AddHorizontalLayoutGroup("Footer")
                .SetSpreadChildrenHorizontally()
                .SetChildAlignment(TextAnchor.MiddleCenter)
                .AddContent(mountPoint =>
                {
                    mountPoint.AddTextButton("Previous")
                        .OnClick(() => window.ShowCategory(ShelfCategories.GetPreviousCategoryId(this.categoryId)))
                        .SetUIText("UI_PREVIOUS");

                    mountPoint.AddLayoutItem("Spacer")
                        .SetExpandWidth();

                    mountPoint.AddTextButton("BackButton")
                        .SetUIText("UI_BACK")
                        .OnClick(() => this.window.ShowCategories());

                    mountPoint.AddLayoutItem("Spacer")
                        .SetExpandWidth();

                    mountPoint.AddTextButton("Next")
                        .OnClick(() => window.ShowCategory(ShelfCategories.GetNextCategoryId(this.categoryId)))
                        .SetUIText("UI_NEXT");
                });
        }

        private void BuildShelfList(WidgetMountPoint mountPoint)
        {
            var list = (WidgetMountPoint)mountPoint.AddVerticalLayoutGroup("Shelves")
                .SetExpandWidth()
                .SetFitContentHeight()
                .SetSpreadChildrenHorizontally()
                .SetPadding(0, 10)
                .SetSpacing(30);

            var category = ShelfCategories.GetCategory(this.categoryId);
            var shelves =
                from entity in category.GetShelves()
                orderby entity.Name
                select entity;

            // Shame we don't have a good windowing function built into .net
            foreach (var partition in shelves.Partition(3))
            {
                list.AddHorizontalLayoutGroup()
                    .SetExpandWidth()
                    .SetFitContentHeight()
                    .SetSpacing(30)
                    .SetChildAlignment(TextAnchor.UpperCenter)
                    .AddContent(mountPoint =>
                    {
                        foreach (var item in partition)
                        {
                            BuildShelfItem(item, mountPoint);
                        }
                    });
            }
        }

        private void BuildShelfItem(ShelfEntity entity, WidgetMountPoint mountPoint)
        {
            mountPoint.AddVerticalLayoutGroup($"Shelf_{entity.Id}")
                .SetSpacing(10)
                .AddContent(mountPoint =>
                {
                    mountPoint.AddLayoutItem()
                        .SetPreferredWidth(100)
                        .SetPreferredHeight(100)
                        .WithBehavior<GlowOnHover>()
                        .AddContent(mountPoint =>
                        {
                            var icon = mountPoint.AddImage()
                                .SetPreferredWidth(100)
                                .SetPreferredHeight(100)
                                .CenterImage()
                                .OnBeginDrag(e => OnDragShelfStart(entity.Id, e))
                                .OnContinueDrag(this.OnDragShelf)
                                .OnEndDrag(this.OnDragShelfEnd)
                                .SetSprite(entity.GetIconSprite() ?? ShelfCategories.GetCategory(this.categoryId).Sprite)
                                .SetColor(Shelves.GetOpenShelves().Contains(entity.Id) ? new Color(0.3f, 0.3f, 0.3f, 1) : Color.white);
                            this.shelfIcons.Add(entity.Id, icon);
                        });

                    mountPoint.AddText()
                        .SetFontSize(20)
                        .SetPreferredWidth(100)
                        .SetHorizontalAlignment(TMPro.HorizontalAlignmentOptions.Center)
                        .SetVerticalAlignment(TMPro.VerticalAlignmentOptions.Middle)
                        .SetText(entity.Name);
                });
        }

        private void OnDragShelfStart(string shelfId, PointerEventData eventData)
        {
            if (Shelves.GetOpenShelves().Contains(shelfId))
            {
                // There Can Only Be One
                return;
            }

            var tabletop = GameAPI.GetTabletopSphere();
            if (tabletop == null || !tabletop.IsValid())
            {
                return;
            }

            // After a game is saved, exited, and restarted, this starts returning spheres with a null choreographer that crash on AcceptToken.
            // // var enRouteSphere = Watchman.Get<HornedAxe>().GetSpheres().OfType<EnRouteSphere>().FirstOrDefault();
            // if (enRouteSphere == null || !enRouteSphere.IsValid())
            // {
            //     return;
            // }

            var zoneCreationCommand = new ShelfCreationCommand(shelfId);

            var shelf = zoneCreationCommand.Execute(Context.Unknown());
            if (!shelf.IsValid())
            {
                return;
            }

            var token = Watchman.Get<PrefabFactory>().CreateLocally<Token>(tabletop.GetRectTransform());

            token.SetPayload(shelf);

            if (!token.IsValid())
            {
                return;
            }

            var enRouteSphere = tabletop.GetEnRouteSphere();
            if (enRouteSphere == null || !enRouteSphere.IsValid())
            {
                return;
            }

            // We have to be in something to start the drag, and that something controls our mouse offset.
            // enroute sphere works nicely for that purpose.
            enRouteSphere.AcceptToken(token, new Context(Context.ActionSource.PlayerDrag));

            // Sync the token to our mouse position.
            RectTransformUtility.ScreenPointToWorldPointInRectangle(tabletop.GetRectTransform(), eventData.pressPosition, eventData.pressEventCamera, out var worldPoint);
            token.transform.position = worldPoint;

            token.OnBeginDrag(eventData);

            draggedShelf = token;
        }

        private void OnDragShelf(PointerEventData eventData)
        {
            if (draggedShelf == null)
            {
                return;
            }

            draggedShelf.OnDrag(eventData);
        }

        private void OnDragShelfEnd(PointerEventData eventData)
        {
            if (draggedShelf == null)
            {
                return;
            }
            draggedShelf.OnEndDrag(eventData);
            draggedShelf = null;
        }
    }
}
