using Roost.Piebald;
using SecretHistories.UI;

namespace Shelf
{
    public class ShelvesWindow : AbstractTableWindow
    {
        private IShelvesView view;

        protected override int DefaultWidth => 500;

        protected override int DefaultHeight => 900;

        public void ShowCategories()
        {
            this.Clear();

            this.view = new ShelvesCategoryListView(this, this.Icon, this.Content, this.Footer);
        }

        public void ShowCategory(string categoryId)
        {
            this.Clear();

            this.view = new ShelvesCategoryView(this, categoryId, this.Icon, this.Content, this.Footer);
        }

        // No this isnt an override.  Unity names "Awake" without "on" but doenst do the same for "OnDestroy"
        // We want to cascade OnAwake down the inheritance chain, but doing so for OnDestroy would name conflict.
        // It sucks, but not dealing with that now.  Perhaps we need an OnOnDestroy....
        public void OnDestroy()
        {
            this.Clear();
        }

        protected override void OnOpen()
        {
            this.ShowCategories();

            base.OnOpen();
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            if (this.view != null)
            {
                this.view.Update();
            }
        }

        protected override void OnAwake()
        {
            base.OnAwake();

            this.Title = Watchman.Get<ILocStringProvider>().Get("UI_SHELVES");
        }

        private new void Clear()
        {
            if (this.view != null)
            {
                this.view.OnDestroy();
                this.view = null;
            }

            base.Clear();
        }
    }
}
